import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    'Chad',
                    style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                ),
                Text(
                  'Laid to rest June 13th, 2020',
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
    Color color = Theme.of(context).primaryColor;
    Widget buttonSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColumn(color, Icons.mood, 'Will Cuddle'),
          _buildButtonColumn(color, Icons.access_time, 'Loves to Sleep'),
          _buildButtonColumn(color, Icons.mood_bad, 'Will Bite!'),
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        'This was my pup Chad! He loved to be held and cuddled with people '
        'he knows, but beware! He is a fiesty boy! Here is another picture '
        'of him when he was super small and a couple of icons that describe '
        'him!',
        softWrap: true,
      ),
    );
    return MaterialApp(
      title: 'Chad App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Chad App'),
        ),
        body: ListView(
          children: [
            Image.asset(
              'images/chad.jpeg',
              width: 460,
              height: 520,
              fit: BoxFit.cover,
            ),
            titleSection,
            textSection,
            buttonSection,
            Image.asset(
              'images/chadbaby.jpeg',
              fit: BoxFit.fill,
            ),
          ],
        ),
      ),
    );
  }

  Column _buildButtonColumn(Color color, IconData icon, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(icon, color: color),
        Container(
          margin: const EdgeInsets.only(top: 8, bottom: 10),
          child: Text(
            label,
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.w400,
              color: color,
            ),
          ),
        ),
      ],
    );
  }
} //MyApp
